Jana Group Code of Conduct
==========================

Working together in a respectful and effective manner.

Workspace State
---------------
* Keep a tidy workspace.
* Keep tools in the toolbox if not in active use.
* No unlabeled vials allowed.
* For multi-day work, at the end of each day, clean up your workspace and leave
  a note with your name.
* Keep all instruments and equipment ready for use, even if they are under
  repair.
* Every mass-spec instrument must ready for use after a syringe is inserted (no
  missing tubing, or ferrules).
* Notify others if you see clutter.

(Semi) Consumables
------------------
Keep a track of usage to avoid delays.

* Order a new silica capillary (tell Theo) and leave a note if there is only
  one spool left.
* Order more vials/caps if there are less than 3 boxes of 4ml vials/2 sacks of
  caps.
* Restock gloves, aluminum foil, tissues, needles and syringes from Jan
  Dommerholt when finished.
* Get a new bag of pipetting tips from Samuel Bosma if there are less than
  approximately 100 left in the last zip bag.

Synthetic Lab
-------------
* Follow the workspace state guide.
* Clean glassware as soon as possible or at least once a day.
* Do not leave dirty glassware near the sink.
* Unlabeled samples in the fridge are not allowed.
* Return the chemicals back to their Labservant locations as soon as possible
  or at least once a day.
* If you finish a solvent bottle, write it on the whiteboard.
* Order solvents when they are running low.
* Always order a 10-liter Acetone (washing) canister when you open a new one.

Feedback
--------
We all make mistakes, and it is important to treat each other with kindness.
Giving and receiving feedback is a crucial part of that. If you notice a mess,
please speak up instead of ignoring it. Receiving feedback provides an
opportunity to improve. Please be kind when giving a feedback and focus on the
behavior, not the person.
